##RecodeByPoly

This is a Python Add-In for **ArcGIS 10.1*** that allows a user to draw a polygon on a raster and recode all of the pixels within the polygon.

####Installation

1. Download [RecodeByPoly.esriaddin][1], double-click on it, and choose *Install Add-In* when prompted. 
2. Select *Extensions* from the ArcGIS *Customize* menu and turn on the *Recode* extension. 
3. If you do not see the toolbar, select *Toolbars* from the *Customize* menu and make sure the *Recode* toolbar is turned on.

####Usage

Once you've added an integer raster to your data frame, the *Layer* menu in the toolbar should become active. Select the layer that you want to edit. The *Value* menu will then be populated with the list of pixel values in the raster. Choose the value that you want to recode things as. Then select either the box or polygon tool and draw a shape over the area that you want to recode. Depending on the size of the raster, you may have to wait a while for it to recode. You will also see things disappear and reappear from the TOC before it's done. Changes are written to the raster immediately, so **make sure you have a backup before you begin!**

![toolbar][2]

[1]: https://bitbucket.org/rsgis/recodebypoly/downloads/RecodeByPoly.esriaddin
[2]: https://bitbucket.org/rsgis/recodebypoly/raw/4936e0efef19cf4d7fb9b48c84f0d69fc7afa196/toolbar.png
