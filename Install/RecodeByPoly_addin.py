import os
import tempfile

import arcpy
import pythonaddins

class LayerComboBox(object):
    """Implementation for RecodeByPoly_addin.layer_combobox (ComboBox)"""
    def __init__(self):
        self.editable = True
        self.enabled = False
        self.dropdownWidth = 'WWWWWWWWWWWWWWWWWWWW'
        self.width = 'WWWWWWWWWWWWWWWWWWWW'
        self.layer = None
    def onSelChange(self, selection):
        if isinstance(selection, arcpy.mapping.Layer):
            self.layer = PsuedoLayer(selection.name, selection.dataSource)
        else:
            self.layer = None
        value_combobox.refresh()
    def onEditChange(self, text):
        pass
    def onFocus(self, focused):
        pass
    def onEnter(self):
        pass
    def refresh(self):
        self.enabled = len(self.items) > 0
    def add(self, lyr):
        if RecodeHelper.isIntegerRaster(lyr):
            self.items.append(lyr)
            self.items.sort(key=lambda(v):v.name)
            self.refresh()
    def addAll(self):
        width = 20
        mxd = arcpy.mapping.MapDocument('CURRENT')
        for lyr in arcpy.mapping.ListLayers(mxd, None, mxd.activeDataFrame):
            if RecodeHelper.isIntegerRaster(lyr):
                self.items.append(lyr)
                width = max(width, len(lyr.datasetName))
        self.items.sort(key=lambda(v):v.name)
        self.dropdownWidth = 'W' * width
        self.enabled = len(self.items) > 0
    def clear(self):
        self.items = []
        self.value = ''
        self.layer = None
        self.refresh()
        value_combobox.clear()
    def remove(self, lyr):
        for item in self.items:
            if RecodeHelper.layersEqual(lyr, item):
                self.items.remove(item)
                break
        if RecodeHelper.layersEqual(lyr, self.layer):
            self.value = ''
        self.refresh()
    def restoreSelection(self, psuedolayer):
        self.layer = psuedolayer
        self.value = psuedolayer.name
        self.refresh()

class PolygonRecode(object):
    """Implementation for RecodeByPoly_addin.poly_tool (Tool)"""
    def __init__(self):
        self.enabled = True
        self.shape = "Line" # Can set to "Line", "Circle" or "Rectangle" for interactive shape drawing and to activate the onLine/Polygon/Circle event sinks.
        self.cursor = 3
    def onMouseDown(self, x, y, button, shift):
        pass
    def onMouseDownMap(self, x, y, button, shift):
        pass
    def onMouseUp(self, x, y, button, shift):
        pass
    def onMouseUpMap(self, x, y, button, shift):
        pass
    def onMouseMove(self, x, y, button, shift):
        pass
    def onMouseMoveMap(self, x, y, button, shift):
        pass
    def onDblClick(self):
        pass
    def onKeyDown(self, keycode, shift):
        pass
    def onKeyUp(self, keycode, shift):
        pass
    def deactivate(self):
        pass
    def onCircle(self, circle_geometry):
        pass
    def onLine(self, line_geometry):
        psuedolayer = layer_combobox.layer
        recoder = Recoder(psuedolayer)
        recoder.recodePolygon(arcpy.Polygon(line_geometry.getPart()), int(value_combobox.value))
        layer_combobox.restoreSelection(psuedolayer)
    def onRectangle(self, rectangle_geometry):
        pass

class RecodeExtension(object):
    """Implementation for RecodeByPoly_addin.recode_extension (Extension)"""
    def __init__(self):
        # For performance considerations, please remove all unused methods in this class.
        self.enabled = True
    def openDocument(self):
        layer_combobox.addAll()
    def closeDocument(self):
        layer_combobox.clear()
    def itemAdded(self, new_item):
        layer_combobox.add(new_item)
    def itemDeleted(self, deleted_item):
        layer_combobox.remove(deleted_item)

class RectangleRecode(object):
    """Implementation for RecodeByPoly_addin.rectangle_tool (Tool)"""
    def __init__(self):
        self.enabled = True
        self.shape = "Rectangle" # Can set to "Line", "Circle" or "Rectangle" for interactive shape drawing and to activate the onLine/Polygon/Circle event sinks.
        self.cursor = 3
    def onMouseDown(self, x, y, button, shift):
        pass
    def onMouseDownMap(self, x, y, button, shift):
        pass
    def onMouseUp(self, x, y, button, shift):
        pass
    def onMouseUpMap(self, x, y, button, shift):
        pass
    def onMouseMove(self, x, y, button, shift):
        pass
    def onMouseMoveMap(self, x, y, button, shift):
        pass
    def onDblClick(self):
        pass
    def onKeyDown(self, keycode, shift):
        pass
    def onKeyUp(self, keycode, shift):
        pass
    def deactivate(self):
        pass
    def onCircle(self, circle_geometry):
        pass
    def onLine(self, line_geometry):
        pass
    def onRectangle(self, rectangle_geometry):
        psuedolayer = layer_combobox.layer
        recoder = Recoder(psuedolayer)
        recoder.recodeRectangle(rectangle_geometry, int(value_combobox.value))
        layer_combobox.restoreSelection(psuedolayer)

class ValueComboBox(object):
    """Implementation for RecodeByPoly_addin.value_combobox (ComboBox)"""
    def __init__(self):
        self.editable = True
        self.enabled = False
        self.dropdownWidth = 'WWWW'
        self.width = 'WWWW'
        self.value = ''
    def onSelChange(self, selection):
        rectangle_tool.enabled = True
        poly_tool.enabled = True
    def onEditChange(self, text):
        pass
    def onFocus(self, focused):
        pass
    def onEnter(self):
        pass
    def refresh(self):
        if layer_combobox.layer == None:
            self.items = []
            self.value = ''
            self.enabled = False
        else:
            self.items = []
            for row in arcpy.da.SearchCursor(layer_combobox.layer.dataSource, 'Value'):
                self.items.append(row[0])
            if RecodeHelper.asInt(self.value) not in self.items:
                self.value = ''
            self.enabled = True
        rectangle_tool.enabled = self.value != ''
        poly_tool.enabled = self.value != ''
    def clear(self):
        self.items = []
        self.value = ''
        self.enabled = False
        self.refresh()
        rectangle_tool.enabled = False
        poly_tool.enabled = False

class PsuedoLayer:
    def __init__(self, name, dataSource):
        self.name = name
        self.dataSource = dataSource

class RecodeHelper:
    @staticmethod
    def asInt(s):
        try:
            return int(s)
        except ValueError:
            return None
    @staticmethod
    def isIntegerRaster(lyr):
        return lyr.isRasterLayer and arcpy.Raster(lyr.dataSource).isInteger
    @staticmethod
    def layersEqual(lyr1, lyr2):
        if (isinstance(lyr1, arcpy.mapping.Layer) or isinstance(lyr1, PsuedoLayer)) and \
           (isinstance(lyr2, arcpy.mapping.Layer) or isinstance(lyr2, PsuedoLayer)):
                return lyr1.name == lyr2.name and lyr1.dataSource == lyr2.dataSource
        elif isinstance(lyr1, str) and isinstance(lyr2, str):
                return lyr1 == lyr2
        elif isinstance(lyr1, str) and \
             (isinstance(lyr2, arcpy.mapping.Layer) or isinstance(lyr2, PsuedoLayer)):
                return lyr1 == lyr2.name
        elif (isinstance(lyr1, arcpy.mapping.Layer) or isinstance(lyr1, PsuedoLayer)) and \
             isinstance(lyr2, str):
                return lyr1.name == lyr2
        else:
                return False        

class Recoder:
    def __init__(self, psuedolayer):
        self.psuedolayer = psuedolayer
        self.raster_lyr = None
        mxd = arcpy.mapping.MapDocument('CURRENT')
        self.df = mxd.activeDataFrame
        for lyr in arcpy.mapping.ListLayers(mxd, psuedolayer.name, mxd.activeDataFrame):
            if RecodeHelper.layersEqual(psuedolayer, lyr):
                self.raster_lyr = lyr
                break
    def saveSettings(self):
        self.addOutputs = arcpy.env.addOutputsToMap
        self.overwrite = arcpy.env.overwriteOutput
        arcpy.env.addOutputsToMap = False
        arcpy.env.overwriteOutput = True
    def restoreSettings(self):
        arcpy.env.addOutputsToMap = self.addOutputs
        arcpy.env.overwriteOutput = self.overwrite
    def makeLyrFile(self):
        tmp = tempfile.mkstemp('.lyr')
        os.close(tmp[0])
        self.raster_lyr.saveACopy(tmp[1])
        return tmp[1]
    def getTempRasterName(self):
        tmp = tempfile.mkstemp('.tif')
        os.close(tmp[0])
        return tmp[1]
    def addLyrFile(self, lyr_file):
        arcpy.mapping.AddLayer(self.df, arcpy.mapping.Layer(lyr_file))
        os.remove(lyr_file)

    def copyRaster(self, temp_file):
        arcpy.mapping.RemoveLayer(self.df, self.raster_lyr)   
        del self.raster_lyr       
        arcpy.CopyRaster_management(temp_file, self.psuedolayer.dataSource)
        arcpy.Delete_management(temp_file)
    def recodePolygon(self, poly, value):
        if self.raster_lyr is None:
            return        
        pts = []
        for pt in poly.getPart(0):
            pts.append(pt) 
        self.recode(arcpy.sa.ExtractByPolygon, pts, value)
    def recodeRectangle(self, rectangle, value):
        if self.raster_lyr is None:
            return        
        self.recode(arcpy.sa.ExtractByRectangle, rectangle, value)
    def recode(self, func, geom, value):
        if self.raster_lyr is None:
            return        
        self.saveSettings()
        lyr_file = self.makeLyrFile()
        raster = arcpy.Raster(self.psuedolayer.dataSource)
        recode_cells = func(raster, geom)
        raster = arcpy.sa.Con(arcpy.sa.IsNull(recode_cells), raster, value)
        arcpy.AddField_management(raster, "hectares", "DOUBLE", 12, 2)
        eq = '!count! * {0} * {1} / 10000'.format(raster.meanCellHeight, raster.meanCellWidth)
        arcpy.CalculateField_management(raster, "hectares", eq, "PYTHON_9.3")
        temp_file = arcpy.CreateScratchName()
        raster.save(temp_file)
        self.copyRaster(temp_file)
        self.addLyrFile(lyr_file)
        self.restoreSettings()
